let btns = document.querySelectorAll(".btn");

document.addEventListener ("keydown", (event) => {

  btns.forEach((btn) => {
    if (btn.classList.contains("highlighter")) {
      btn.classList.remove("highlighter");
    } else if (`Key${btn.dataset.key}` == event.code ||
      (btn.dataset.key == "Enter" && event.key == "Enter")) {
      btn.classList.add("highlighter");
    }
  });

  })

